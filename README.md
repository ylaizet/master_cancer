# Cancer DataBases

Tertiary analysis with **variant interpretation** and its **biological impact**.

![](pipeline.png)

## Key concepts reminder

- Gene structure [wikipedia](https://en.wikipedia.org/wiki/Gene_structure)
- Transcripts & splicing
- Polymorphism / mutation
- Somatic / constitutional
- Oncogene / TSG (tumour suppressor gene)


## Starting material

Files:

- BED files: regions (capture, amplicons, ...)
- BAM files: mapped reads
- VCF files: alterations (variants, ...)

## Annotation tools

- [Annovar](http://annovar.openbioinformatics.org/en/latest/user-guide/download/)
    - [bases](http://annovar.openbioinformatics.org/en/latest/user-guide/download/)
    - [online version](http://wannovar.wglab.org/)
    - [example](http://wannovar.wglab.org/example.html)
- [VEP](https://www.ensembl.org/info/docs/tools/vep/index.html)
    - [bases](https://www.ensembl.org/info/docs/tools/vep/script/vep_cache.html)
    - [online version](https://www.ensembl.org/Multi/Tools/VEP?db=core)
- [SnpEff](https://pcingola.github.io/SnpEff/snpeff/introduction/)

=> `Annotated VCF file`


### Splicing prediction tools

- [SPiP](https://github.com/raphaelleman/SPiP) (splicing prediction tool)
- [MaxEntScan](http://hollywood.mit.edu/burgelab/maxent/Xmaxentscan_scoreseq.html)
- [SpliceAI](https://github.com/Illumina/SpliceAI)
- [AbSplice](https://github.com/gagneurlab/absplice)
- [SpliceVarDB](https://compbio.ccia.org.au/splicevardb/): A comprehensive database of experimentally validated human splicing variants
- [MutSpliceDB](https://brb.nci.nih.gov/cgi-bin/splicing/splicing_main.cgi)


### Annotation types

- name of the gene
- transcript of the gene
- gene region (exon nb, intron nb, ...)
- HGVS nomenclature
- type of alteration (missense, stop, ...)
- in silico predicted impact on protein activity
- splicing sites
- protein domain (Pfam)
- % of alteration in populations
- reference in cancer databases
- ...


##### Variant description

- Genomic coordinates (VCF like)
    - [file format](https://samtools.github.io/hts-specs/VCFv4.5.pdf)
- HGVS Nomenclature

### Genomic coordinates

Relative to genome build : hg19 (GRCh37), hg38 (current)

| Chromosome | Start  | Stop   | Reference | Alternative |
|------------|--------|--------|-----------|-------------|
| chr12      | 125478 | 125478 | A         | T           |

#### Conversion and mapping between assemblies

- unique
    - [broadinstitute](https://liftover.broadinstitute.org/)
    - [genebe](https://genebe.net/tools/liftover)
- batch
    - [ensembl](https://www.ensembl.org/Homo_sapiens/Tools/AssemblyConverter?db=core)

### HGVS Nomenclature

[varnomen](https://varnomen.hgvs.org/)

HGVS-nomenclature is used to report and exchange information regarding variants found in DNA, RNA and protein sequences and serves as an international standard.

- Genome build (hg19, hg38)


- [Nomenclature](https://hgvs-nomenclature.org/stable/background/simple/) : `reference sequence` . `version number` : `description`
    - HGVSg : `NC_000011.10:g.112088971G>T`
    - HGVSc : `NM_003002.3:c.274G>T`
    - HGVSp : `NM_003002.3:p.(Asp92Tyr)`


- Nomenclature check and conversion tools
    - [Mutalyzer](https://mutalyzer.nl/) (Name checker, Position Converter)
    - [VariantValidator](https://variantvalidator.org/) (Validator)
    - [Ensembl variant recoder](https://www.ensembl.org/Homo_sapiens/Tools/VR?db=core)

### Reference sequences

- [NCBI RefSeq](https://www.ncbi.nlm.nih.gov/refseq/)
- [Ensembl](https://www.ensembl.org/Homo_sapiens/Info/Index?db=core)

>[Why do they differ?](https://m.ensembl.org/Help/Faq?id=294)
>
>**While Ensembl gene models are annotated directly on the reference genome, RefSeq annotates on mRNA sequences**. Due to sequence differences between the reference genomes and individual mRNAs, some of the RefSeq mRNAs may not map perfectly to the reference genome. For example, translations may contain stop codons when they are translated from the reference genome's DNA. Ensembl transcripts will reflect the reference genome in these cases, not the mRNA, and therefore there can be small differences between RefSeq mRNA/proteins and Ensembl transcripts/proteins.

#### Matching genes models

- [Match RefSeq and Ensembl](https://www.ncbi.nlm.nih.gov/books/NBK50679/#RefSeqFAQ.how_can_i_identify_matches_bet) or HGNC, OncoKb, ...

**Stable reference**

- [Mane : matching RefSeq and Ensembl transcripts](https://www.ncbi.nlm.nih.gov/refseq/MANE/)
For more information on MANE see [Ensembl’s MANE blog](https://www.ensembl.info/2019/03/12/coming-soon-mane-select-v0-5/) and [NCBI’s MANE blog](https://ncbiinsights.ncbi.nlm.nih.gov/2019/07/03/new-human-genome-annotation-release-with-mane-select-and-other-improvements/).
>[Why do some transcripts have a 'MANE Select' tag ? ](https://www.lrg-sequence.org/faq/)
>The MANE (Matched Annotation from the NCBI and EMBL-EBI) Project is a joint initiative between EMBL-EBI’s [Ensembl/GENCODE Project](https://www.gencodegenes.org/about.html) and NCBI’s [RefSeq project](https://www.ncbi.nlm.nih.gov/refseq/about/). MANE aims to release a genome-wide transcript set that contains one well-supported transcript per protein-coding locus (MANE Select). All transcripts in the MANE set perfectly align to GRCh38 and represent 100% identity (5’UTR, coding sequence, 3’UTR) between the RefSeq (NM) and corresponding Ensembl (ENST) transcript.
>
>The flag MANE Select has been included in the records to note which transcript is the MANE Select for each gene. Currently, a MANE Select transcript has been defined for ~67% of protein-coding loci, though we aim to achieve genome-wide coverage.
- ~~[LRG](https://www.lrg-sequence.org/) : A Locus Reference Genomic (LRG) record contains stable reference sequences that are used for reporting sequence variants with clinical implications~~


### Importance of reference sequence version number

CDKN2A : Check the position for the same reference sequence but a different version number

https://v2.mutalyzer.nl/position-converter?assembly_name_or_alias=GRCh37&description=NM_058195.3%3Ac.384C%3ET

- NM_000077.4:c.341C>T
- NM_058195.2:c.**507**C>T
- NM_058195.3:c.**384**C>T
- NM_058197.4:c.*264C>T


## Guide lines - interpretation

- [ACMG](https://www.acmg.net/docs/standards_guidelines_for_the_interpretation_of_sequence_variants.pdf) (germline)
- [SOVAD : https://www.sciencedirect.com/science/article/abs/pii/S0959804921005803](https://www.sciencedirect.com/science/article/am/pii/S0959804921005803) (somatic)


## Genes

Informations about genes and their function, and more...

- [HGNC gene](https://www.genenames.org/) (HUGO)
- [NCBI gene](https://www.ncbi.nlm.nih.gov/gene/)
- [GeneCards](https://www.genecards.org/)
- [MyGene](https://mygene.info/)


Cancer related

- [COSMIC Cancer Census](https://cancer.sanger.ac.uk/census)
- [OncoKb](https://www.oncokb.org/cancerGenes)


## DataBases

- [NCBI](https://www.ncbi.nlm.nih.gov/)

### Listing

- [https://www.cancer.gov/research/resources](https://www.cancer.gov/research/resources)
- [https://github.com/seandavi/awesome-cancer-variant-databases](https://github.com/seandavi/awesome-cancer-variant-databases)
- [https://varsome.com/datasources](https://varsome.com/datasources)

### Literature

- [NCBI PubMed](https://www.ncbi.nlm.nih.gov/)
- [Scholar](https://scholar.google.com/)
- [Mastermind](https://www.genomenon.com/mastermind/)
- [NCBI PubTator3](https://www.ncbi.nlm.nih.gov/research/pubtator3/)
- [CancerMine](http://bionlp.bcgsc.ca/cancermine/)


### Common databases

- Global (frequency in global population)
    - [1000g](https://www.internationalgenome.org/)
    - [gnomAD](https://gnomad.broadinstitute.org/)
- Cancer (curation, [interpretation](https://www.ncbi.nlm.nih.gov/clinvar/docs/clinsig/))
    - [COSMIC](https://cancer.sanger.ac.uk/cosmic) ex.: COSM1152503, ~~COSV55756103~~
        > The new Genomic mutation identifier (COSV) is not currently searchable on the website. The alternative mutation ID (which can be found in the overview section) may be used to view the variant on alternative transcripts [Source](https://cancer.sanger.ac.uk/cosmic/variant-updates)
    - [dbsnp](https://www.ncbi.nlm.nih.gov/snp/) ex.: rs9919552
    - [ClinVar](https://www.ncbi.nlm.nih.gov/clinvar/intro/) ex.: 151004
    - [CGI](https://www.cancergenomeinterpreter.org/home) (gene role OG, TSG)
    - [OncoKB](https://www.oncokb.org/)
- Collaborative / specialised on genes
    - [CIVIC](https://civicdb.org/home)
    - [BRCAexchange](https://brcaexchange.org/)
    - UMD (BRCA, MSH2, ...)
      - https://p53.fr/tp53-database/
    - [IARC TP53](https://tp53.cancer.gov/)
    - [LOVD](https://www.lovd.nl/) (curated gene variant databases)
- HUBs
    - [MyVariant](https://myvariant.info/)
    - [VarSome](https://varsome.com/)
    - [MobiDetails](https://mobidetails.iurc.montp.inserm.fr/MD/)
- Impact on protein function
    - polyphen
    - sift
    - CADD : https://cadd.gs.washington.edu/snv
    - [Mutation Taster](https://www.mutationtaster.org/)
    - [AlphaMissense](https://alphamissense.hegelab.org/search)
    - et [autres](https://grch37.ensembl.org/info/genome/variation/prediction/protein_function.html) ... 
    - [ESM](https://github.com/facebookresearch/esm)
    - ~~[MISTIC](https://github.com/weber8thomas/MISTIC-public?tab=readme-ov-file)~~
    - [REVEL](https://sites.google.com/site/revelgenomics/)
    - [Metadome](https://stuart.radboudumc.nl/metadome/)
    - [dbnsfp](https://www.dbnsfp.org/)
- Clinical trials
    - [ClinicalTrials.gov](https://clinicaltrials.gov/)
- Protein domains
    - [Pfam](https://pfam.xfam.org/)
    - [uniprot](https://www.uniprot.org/), interpro, prosite
- Drugs
    - [PharmGKB](https://www.pharmgkb.org/)
    - [DGIdb](http://www.dgidb.org/)
- Fusions
    - [Mitelman](https://mitelmandatabase.isb-cgc.org/mb_search)
    - [GDB](https://compbio.uth.edu/FusionGDB2/)
    - [Quiver](http://quiver.archerdx.com/)
    - ~~[Readthrough](https://metasystems.riken.jp/conjoing/)~~
- structural variations
    - [dbvar](https://www.ncbi.nlm.nih.gov/dbvar/)
- cluster
  - [3dhotspots](https://www.3dhotspots.org/#/home)
  - [cancerhotspots](https://www.cancerhotspots.org/#/home)

### Genome browser

- [NCBI GDV](https://www.ncbi.nlm.nih.gov/genome/gdv/)
- [UCSC](https://genome.ucsc.edu/cgi-bin/hgTracks?)

### Data

Websites:

- [UCSC table browser](https://www.genome.ucsc.edu/cgi-bin/hgTables)

FTP:

- [NCBI FTP](https://ftp.ncbi.nlm.nih.gov/)
- [Ensembl FTP](https://grch37.ensembl.org/info/data/ftp/index.html)

### Portals

Publicly available omic data with some clinical data

- [cBiopotal](https://www.cbioportal.org/)
- [Genie](https://www.aacr.org/professionals/research/aacr-project-genie/aacr-project-genie-data/)
- [ICGC](https://dcc.icgc.org/)
- [GDC, TCGA](https://portal.gdc.cancer.gov/)
- [Tumor portal](http://www.tumorportal.org)
- https://franklin.genoox.com/clinical-db/home (somactic & germline)
- https://cancervar.wglab.org/
  - CancerVar: an Artificial Intelligence empowered platform for clinical interpretation of somatic mutations in cancer (Science Advances ,2022, https://www.science.org/doi/10.1126/sciadv.abj1624 )
- https://www.genomenexus.org/
- https://ckb.genomenon.com/


# Let's practice

**example vcf** : [link](https://nextcloudib.bordeaux.unicancer.fr/index.php/s/rtGMDxbq9dcwZSp)

- Check reference sequence impact on nomenclature and sequence [example](https://www.ncbi.nlm.nih.gov/nuccore/NM_000546?report=girevhist)
- Check syntax of NM_014804.2:.2687A>G [help](https://v2.mutalyzer.nl/syntax-checker?description=NM_014804.2%3A.2687A%3EG) [help2](https://varsome.com/variant/hg19/NM_014804.2%3Ac.2687T%3EG?annotation-mode=somatic) [help3](https://variantvalidator.org/service/validate/)
- Check name of NM_014804.2:c.2687T>G [help](https://v2.mutalyzer.nl/name-checker?description=NM_014804.2%3Ac.2687T%3EG) [help2](https://varsome.com/variant/hg19/NM_014804.2%3Ac.2687T%3EG?annotation-mode=somatic) [help3](https://mobidetails.iurc.montp.inserm.fr/MD/)
- Pick up a gene and check wether it's cancer related (PTEN, VCL, MDMD2, HES4, ISG15, TP53, AGRN, ALK, ...) [help](https://www.oncokb.org/gene/PTEN)
- Get information on known variant (rs1443417, COSV51398511 / COSM1007540) [help](https://www.ncbi.nlm.nih.gov/snp/rs1443417) [help2](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=102908475&merge=1640835)
    - Get frequence of variant in global population. Do you think it's a polymorphism? (annotated vcf, db)
- Check biological impact of variation on protein function NM_000546.5:c.1024C>T (annotated vcf, db) [help](https://varsome.com/variant/hg19/NM_000546%3Ac.1024C%3ET?) [help2](https://mobidetails.iurc.montp.inserm.fr/MD/api/variant/17099/browser/), [help3](https://www.ncbi.nlm.nih.gov/clinvar/variation/182970/)
- Clinical significance (rs587782664, NM_001276760.3:c.594G>T) [help](https://www.ncbi.nlm.nih.gov/snp/rs587782664#clinical_significance) [help2](https://varsome.com/variant/hg19/rs587782664?annotation-mode=somatic) [help3](https://varsome.com/variant/hg19/NM_001276760%3Ac.594G%3ET?) [help4](https://www.oncokb.org/gene/KRAS/G12C)
- Get genomic coordinates of variant NM_033360.3:c.99T>G [help](https://v2.mutalyzer.nl/position-converter?assembly_name_or_alias=GRCh37&description=NM_033360.2%3Ac.99T%3EG), [help2](https://variantvalidator.org/), [help3](https://mobidetails.iurc.montp.inserm.fr/MD/)
- Convert variant coordinates to another genome or transcript
    - RefSeq to Ensembl
    => NCBI Mane on nucleotide
    https://www.ncbi.nlm.nih.gov/nuccore/NM_000546.6
    - Ensembl to RefSeq => Show transcript table
    https://www.ensembl.org/Homo_sapiens/Transcript/Summary?db=core;g=ENSG00000171862;r=10:87863625-87971930;t=ENST00000371953
    - ENST to RefSeq conversion / match from Ensembl or NCBI (ENSG00000139618, NM_000059.4)
- Get preponderant tumor type for variant in cosmic or oncokb
- PTEN transcripts at [Ensembl](https://www.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g=ENSG00000171862;r=10:87863625-87971930)
- PTEN Gene at [GDC portal](https://portal.gdc.cancer.gov/genes/ENSG00000171862)
- PTEN Gene at [UCSC genome browser](https://www.genome.ucsc.edu/cgi-bin/hgTracks?db=hg19&lastVirtModeType=default&lastVirtModeExtraState=&virtModeType=default&virtMode=0&nonVirtPosition=&position=chr10%3A89623195%2D89728532&hgsid=1052313405_VyaOG6pOIt2maQbKgBbWQhweUtva)
- PTEN gene at [NCBI](https://www.ncbi.nlm.nih.gov/gene/5728)
    - Search Protein accession
    - Look at [Pfam domain legacy](https://pfam.xfam.org/protein/P60484.1) or [Pfam domain](https://www.ebi.ac.uk/interpro/protein/UniProt/P60484/entry/pfam/#table)
- Check KRAS Q22K variant in litterature [help](https://www.ncbi.nlm.nih.gov/research/pubtator3/docsum?text=@VARIANT_p.Q22K_KRAS_human)
- Use HUB api and visualize json response in browser [help](http://myvariant.info/v1/variant/chr5:g.11384935G%3ET)
- Find tumor type of sample LMS25 on ICGC portal
- Test [Varsome](https://varsome.com/variant/hg19/NM_033360%3Ac.99T%3EG?)
- Test [Mobidetails](https://mobidetails.iurc.montp.inserm.fr/MD/api/variant/191153/browser/) for NM_033360:c.99T>G, hg19-17-7577570-C-T

